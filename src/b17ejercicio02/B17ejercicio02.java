package b17ejercicio02;
/**
 * @author mrodriguezmolares
 * @version 2.0
 */
public class B17ejercicio02 {

    public static void main(String[] args) {

        // Reservamos el espacio en memoria.
        int notas[] = new int[30];
        int acumuladorSuspensos = 0;
        int acumuladorAprobados = 0;
        int suma = 0;
        int media = 0;
        int superiores = 0;

        /*Realizamos el bucle for para que nos de las notas aleatoriamente.
         * Realizamos el math.random entre 10 y 0.
         */
        for (int i = 0; i < notas.length; i++) {

            notas[i] = (int) (Math.random() * ((10 - 0 + 1) + 0));
            
            if (notas[i] < 5) {
                acumuladorSuspensos = acumuladorSuspensos + 1;
            } else {
                acumuladorAprobados = acumuladorAprobados + 1;
            }
            suma = suma + notas[i];
            media = suma / notas.length;

            if (notas[i] > media) {
                superiores = notas[i];
            }
        }

            // Calcular la media del curso:

            System.out.println("El total de suspensos: " + acumuladorSuspensos);
            System.out.println("El total de aprobados: " + acumuladorAprobados);
            //Nota Media de los alumnos.
            System.out.println("Nota media del curso: " + media);
            //Notas más altas a la media.
            System.out.println("Listado de notas superiores a la media: " + superiores);

        }
    }